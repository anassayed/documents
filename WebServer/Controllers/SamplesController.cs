﻿using Microsoft.AspNetCore.Mvc;


namespace WebServer.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class SamplesController : ControllerBase
    {
        private static string _documentXml;
        private static string _documentJson;

        public SamplesController()
        {
        }

        [HttpHead("Document2.xml")]
        [HttpGet("Document2.xml")]
        public ActionResult<string> GetDocumentXML()
        {
            string doc = _documentXml ?? "<document></document>";
            return Content(doc, "application/xml");
        }
        [HttpHead("Document2.json")]
        [HttpGet("Document2.json")]
        public ActionResult<string> GetDocumentJson()
        {
            string doc = _documentJson ?? "{}";
            return Content(doc, "application/json");
        }
        [HttpPut("Document2.Xml")]
        public void PutDocumentXml()
        {
            var reader = new System.IO.StreamReader(Request.Body);
            _documentXml = reader.ReadToEnd();
        }
        [HttpPut("Document2.json")]
        public void PutDocumentJson()
        {
            var reader = new System.IO.StreamReader(Request.Body);
            _documentJson = reader.ReadToEnd();
        }
        [HttpPost("Document2.Xml")]
        public void PostDocumentXml()
        {
            var reader = new System.IO.StreamReader(Request.Body);
            _documentXml = reader.ReadToEnd();
        }
        [HttpPost("Document2.json")]
        public void PostDocumentJson()
        {
            var reader = new System.IO.StreamReader(Request.Body);
            _documentJson = reader.ReadToEnd();
        }
    }
}