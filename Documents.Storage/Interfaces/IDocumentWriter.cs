﻿using System.Threading.Tasks;

namespace Documents.Storage.Interfaces
{
    public interface IDocumentWriter
    {
        void Write(string value);
        Task WriteAsync(string value);
    }
}

