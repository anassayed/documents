﻿using System.Threading.Tasks;

namespace Documents.Storage.Interfaces
{
    public interface IDocumentReader
    {
        string ReadToEnd();
        Task<string> ReadToEndAsync();
    }
}
