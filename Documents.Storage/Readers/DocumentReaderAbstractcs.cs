﻿using System;
using System.Threading.Tasks;
using Documents.Storage.Interfaces;

namespace Documents.Storage.Readers
{
    public abstract class DocumentReaderAbstractcs : IDocumentReader
    {
        public string ReadToEnd() => ReadToEndAsync().GetAwaiter().GetResult();

        public abstract Task<string> ReadToEndAsync();
    }
}

