﻿using System;
using System.Threading.Tasks;
using System.Net.Http;
using System.Net.Http.Headers;

namespace Documents.Storage.Readers
{
    public class HttpDocumentReader : DocumentReaderAbstractcs
    {
        private readonly Uri _uri;

        public HttpDocumentReader(Uri uri)
        {
            _uri = uri ?? throw new Exception("Url is null");
        }

        public override async Task<string> ReadToEndAsync()
        {
            using (var client = new HttpClient())
            {
                try
                {
                    return await client.GetStringAsync(_uri);
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }
        }
        public HttpContentHeaders GetHeaders() => GetHeadersAsync().GetAwaiter().GetResult();

        public async Task<HttpContentHeaders> GetHeadersAsync()
        {
            using (var client = new HttpClient())
            {
                try
                {
                    HttpRequestMessage message = new HttpRequestMessage
                    {
                        Method = HttpMethod.Head,
                        RequestUri = _uri,
                    };
                    var res = await client.SendAsync(message);
                    return res.Content.Headers;
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }
        }
    }
}
