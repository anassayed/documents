﻿using System;
using System.IO;
using System.Threading.Tasks;

namespace Documents.Storage.Readers
{
    public class FileDocumentReader: DocumentReaderAbstractcs
    {
        private readonly string _filePath;

        public FileDocumentReader(string filePath)
        {
            _filePath = !string.IsNullOrEmpty(filePath) ? filePath : throw new Exception("File path is empty");
        }

        public override async Task<string> ReadToEndAsync()
        {
            try
            {
                using (var sourceStream = File.Open(_filePath, FileMode.Open, FileAccess.Read))
                using (var reader = new StreamReader(sourceStream))
                {
                    return await reader.ReadToEndAsync();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}