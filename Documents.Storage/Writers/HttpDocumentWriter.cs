﻿using System;
using System.Threading.Tasks;
using System.Net;
using System.Net.Http;

namespace Documents.Storage.Writers
{
    public class HttpDocumentWriter : DocumentWriterAbstractcs
    {
        private readonly Uri _uri;

        public HttpDocumentWriter(Uri uri)
        {
            _uri = uri ?? throw new Exception("Url is null");
        }

        public async override Task WriteAsync(string value)
        {
            using (var client = new HttpClient())
            {
                var requestMessage = new HttpRequestMessage
                {
                    RequestUri = _uri,
                    Method = HttpMethod.Put,
                    Content = new StringContent(value)
                };
                var responsetMessage = await client.SendAsync(requestMessage);
                if (responsetMessage.StatusCode != HttpStatusCode.OK)
                {
                    throw new Exception($"Error {responsetMessage.StatusCode}");
                }
            }
        }
    }
}
