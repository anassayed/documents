﻿using System.Threading.Tasks;
using Documents.Storage.Interfaces;

namespace Documents.Storage.Writers
{
    public abstract class DocumentWriterAbstractcs : IDocumentWriter
    {
        public void Write(string value) => WriteAsync(value).GetAwaiter().GetResult();

        public abstract Task WriteAsync(string value);
    }
}
