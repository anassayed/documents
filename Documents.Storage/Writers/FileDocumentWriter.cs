﻿using System;
using System.IO;
using System.Threading.Tasks;

namespace Documents.Storage.Writers
{
    public class FileDocumentWriter: DocumentWriterAbstractcs
    {
        private readonly string _filePath;

        public FileDocumentWriter(string filePath)
        {
            _filePath = !string.IsNullOrEmpty(filePath) ? filePath : throw new Exception("File path is empty");
        }

        public async override Task WriteAsync(string value)
        {
            try
            {
                using (var stream = File.Open(_filePath, FileMode.Create, FileAccess.Write))
                using (var writer = new StreamWriter(stream))
                {
                    await writer.WriteAsync(value);
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

    }
}