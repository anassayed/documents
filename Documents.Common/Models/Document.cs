﻿using System.Xml.Serialization;

namespace Documents.Common.Models
{

    [XmlRoot("document")]
    public class Document
    {
        [XmlElement("title")]
        public string Title { get; set; }
        [XmlElement("text")]
        public string Text { get; set; }
    }
}
