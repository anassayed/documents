﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json.Serialization;

namespace Documents.Tests
{
    using Documents.Common.Models;
    using Documents.Serializers;

    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestJsonSerializers()
        {
            var document = new Document
            {
                Title = "Document Test",
                Text = "This is Serialization test"
            };
            var jsonDocument = "{\r\n  \"title\": \"Document Test\",\r\n  \"text\": \"This is Serialization test\"\r\n}";
            TestSerializer(new JsonDocumentSerializer<Document>(new CamelCaseNamingStrategy()), document, jsonDocument);
        }

        [TestMethod]
        public void TestXmlSerializers()
        {
            var document = new Document
            {
                Title = "Document Test",
                Text = "This is Serialization test"
            };
            var xmldocument = "<?xml version=\"1.0\" encoding=\"utf-16\"?>\r\n<document xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\">\r\n  <title>Document Test</title>\r\n  <text>This is Serialization test</text>\r\n</document>";

            TestSerializer(new XmlDocumentSerializer<Document>(), document, xmldocument);
        }

        private void TestSerializer(IDocumentSerializer<Document> serializer, Document document, string textDocument)
        {
            var storage = new StringStorage();
            IDocumentProvider<Document> documentProvider = new DocumentProvider<Document>(serializer, storage, storage);

            documentProvider.Save(document);
            var docSaved = storage.ReadToEnd();
            Assert.AreEqual(docSaved, textDocument);

            var docResult = documentProvider.Load();
            Assert.AreEqual(document.Title, docResult.Title);
            Assert.AreEqual(document.Text, docResult.Text);
        }
    }
}
