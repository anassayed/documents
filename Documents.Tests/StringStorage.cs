﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Documents.Storage.Interfaces;

namespace Documents.Tests
{
    public class StringStorage : IDocumentReader, IDocumentWriter
    {
        private string _document = string.Empty;

        public string ReadToEnd() => _document;

        public Task<string> ReadToEndAsync() => Task.Run(ReadToEnd);

        public void Write(string value) => _document = value;

        public Task WriteAsync(string value) => Task.Run(()=>Write(value)) ;

    }
}
