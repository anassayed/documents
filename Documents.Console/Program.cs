﻿using System;
using System.IO;
using Documents.Common.Models;
using Documents.Serializers;
using Documents.Storage.Interfaces;
using Documents.Storage.Readers;
using Documents.Storage.Writers;
using Newtonsoft.Json.Serialization;

namespace Documents
{
    class Program
    {
        static void Main(string[] args)
        {
            var sourceFolder = Path.Combine(Environment.CurrentDirectory, @"..\..\..\Source Files");
            var targetFolder = Path.Combine(Environment.CurrentDirectory, @"..\..\..\Target Files");

            LoadAndView("Load fix Json from cloud", "http://www.sayed.cz/samples/Document1.json");
            LoadAndView("Load fix XML from cloud", "http://www.sayed.cz/samples/Document1.xml");
            LoadAndView("Load json from FileSystem", Path.Combine(sourceFolder, "Document1.json"));
            LoadAndView("Load XML from FileSystem", Path.Combine(sourceFolder, "Document1.xml"));
            var document = new Document();
            document.Title = "Document2";

            document.Text = "This document are saved in the file 'Document2.json'";
            SaveAndView("Save json to FileSystem", document, Path.Combine(targetFolder, "Document2.json"));

            document.Text = "This document are saved in the file 'Document2.xml'";
            SaveAndView("Save xml to FileSystem", document, Path.Combine(targetFolder, "Document2.xml"));

            document.Text = "This document are upload to cloud in json format";
            SaveAndView("Upload json to cloud", document, "https://localhost:5001/samples/Document2.json", "application/json");

            document.Text = "This document are upload to cloud in xml format";
            SaveAndView("Upload xml to cloud", document, "https://localhost:5001/samples/Document2.xml", "application/xml");

            LoadAndView("Load Json from cloud", "https://localhost:5001/samples/Document2.json");
            LoadAndView("Load XML from cloud", "https://localhost:5001/samples/Document2.xml");

        }

        static void LoadAndView(string Title, string fileAddress)
        {
            var doc = Load<Document>(fileAddress);
            Console.WriteLine(Title);
            Console.WriteLine("--------------------------------------------");
            Console.WriteLine($"Title : {doc.Title}");
            Console.WriteLine($"Text: {doc.Text}");
            Console.WriteLine("============================================\r\n");
        }

        static void SaveAndView(string Title, Document document, string fileAddress, string contentType=null)
        {
            Save<Document>(document, fileAddress, contentType);
            Console.WriteLine(Title);
            Console.WriteLine("--------------------------------------------");
            Console.WriteLine($"Title : {document.Title}");
            Console.WriteLine($"Text: {document.Text}");
            Console.WriteLine("============================================\r\n");
        }

        static void Save<TDocument>(TDocument document, string fileAddress, string contentType) where TDocument : class
        {
            IDocumentWriter writer;
            IDocumentSerializer<TDocument> serializer;

            Uri uri = new Uri(fileAddress);
            switch (uri.Scheme)
            {
                case "file":
                    writer = new FileDocumentWriter(uri.OriginalString);
                    var fileInfo = new FileInfo(uri.OriginalString);
                    switch (fileInfo.Extension.ToLower())
                    {
                        case ".json":
                            serializer = new JsonDocumentSerializer<TDocument>(new CamelCaseNamingStrategy());
                            break;
                        case ".xml":
                            serializer = new XmlDocumentSerializer<TDocument>();
                            break;
                        default:
                            return;
                    }
                    break;
                case "http":
                case "https":
                    writer = new HttpDocumentWriter(uri);
                    switch (contentType)
                    {
                        case "application/json":
                            serializer = new JsonDocumentSerializer<TDocument>(new CamelCaseNamingStrategy());
                            break;
                        case "application/xml":
                            serializer = new XmlDocumentSerializer<TDocument>();
                            break;
                        default:
                            return;
                    }
                    break;

                default: return;
            }
            var doc = new DocumentProvider<TDocument>(serializer, null, writer);
            doc.Save(document);
        }

        static TDocument Load<TDocument>(string fileAddress) where TDocument : class
        {
            IDocumentReader reader;
            IDocumentSerializer<TDocument> serializer;
            Uri uri = new Uri(fileAddress);

            switch (uri.Scheme)
            {
                case "file":
                    reader = new FileDocumentReader(uri.OriginalString);
                    var fileInfo = new FileInfo(uri.OriginalString);
                    switch (fileInfo.Extension.ToLower())
                    {
                        case ".json":
                            serializer = new JsonDocumentSerializer<TDocument>(new CamelCaseNamingStrategy());
                            break;
                        case ".xml":
                            serializer = new XmlDocumentSerializer<TDocument>();
                            break;
                        default:
                            return null;
                    }

                    break;
                case "http":
                case "https":
                    var httpreader = new HttpDocumentReader(uri);
                    reader = httpreader;
                    var h = httpreader.GetHeaders();
                    switch (h.ContentType.MediaType)
                    {
                        case "application/json":
                            serializer = new JsonDocumentSerializer<TDocument>(new CamelCaseNamingStrategy());
                            break;
                        case "application/xml":
                            serializer = new XmlDocumentSerializer<TDocument>();
                            break;
                        default:
                            return null;
                    }

                    break;
                default:
                    return null;
            }

            var doc = new DocumentProvider<TDocument>(serializer, reader, null);
            return doc.Load();
        }
    }
}
