﻿namespace Documents
{
    public interface IDocumentProvider<TDocument>
    {
        bool CanRead { get; }
        bool CanWrite { get; }

        void Save(TDocument document);
        TDocument Load();
    }
}
