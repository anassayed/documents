﻿using System;
using Documents.Serializers;
using Documents.Storage.Interfaces;

namespace Documents
{
    public class DocumentProvider<TDocument> : IDocumentProvider<TDocument>
    {
        private readonly IDocumentSerializer<TDocument> _documentSerializer;
        private readonly IDocumentReader _documentReader;
        private readonly IDocumentWriter _documentWriter;

        public DocumentProvider(
                IDocumentSerializer<TDocument> documentSerializer,
                IDocumentReader documentReader,
                IDocumentWriter documentWriter
            )
        {
            _documentSerializer = documentSerializer;
            _documentReader = documentReader;
            _documentWriter = documentWriter;
        }

        public bool CanRead => _documentReader != null;
        public bool CanWrite => _documentWriter != null;

        public TDocument Load()
        {
            if (!CanRead) throw new Exception("You can't load the document");
            var textBody = _documentReader.ReadToEnd();
            var document = _documentSerializer.Deserialize(textBody);
            return document;
        }

        public void Save(TDocument document)
        {
            if (!CanWrite) throw new Exception("You can't save the document");
            var textBody = _documentSerializer.Serialize(document);
            _documentWriter.Write(textBody);

        }

       
    }
}
