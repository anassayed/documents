﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;


namespace Documents.Serializers
{
    public class JsonDocumentSerializer<TValue> : IDocumentSerializer<TValue>
    {
        private readonly NamingStrategy _namingStrategy;

        public JsonDocumentSerializer()
        {
            _namingStrategy = new DefaultNamingStrategy();
        }
        public JsonDocumentSerializer(NamingStrategy namingStrategy)
        {
            _namingStrategy = namingStrategy;
        }
        public TValue Deserialize(string value)
        {
            var document = (TValue)JsonConvert.DeserializeObject(value, typeof(TValue));
            return document;
        }

        public string Serialize(TValue document)
        {
            DefaultContractResolver contractResolver = new DefaultContractResolver
            {
                NamingStrategy = _namingStrategy
            };
            JsonSerializerSettings jsonSerializerSettings = new JsonSerializerSettings
            {
                ContractResolver = contractResolver,
                Formatting = Formatting.Indented
            };

            return JsonConvert.SerializeObject(document, jsonSerializerSettings);
        }
    }
}
