﻿namespace Documents.Serializers
{ 
    public interface IDocumentSerializer<T>
    {
        string Serialize(T document);
        T Deserialize(string value);
    }
}
