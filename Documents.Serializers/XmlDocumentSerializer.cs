﻿using System.Xml.Serialization;
using System.IO; 

namespace Documents.Serializers
{
    public class XmlDocumentSerializer<TValue> : IDocumentSerializer<TValue>
    {

        public TValue Deserialize(string value)
        {
            var serializer = new XmlSerializer(typeof(TValue));
            var stringReader = new StringReader(value);

            return (TValue)serializer.Deserialize(stringReader); ;
        }

        public string Serialize(TValue document)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(TValue));
            var stringWriter = new StringWriter();
            serializer.Serialize(stringWriter, document);

            return stringWriter.ToString();
        }
    }
}
